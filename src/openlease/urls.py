'''
Master URLs file for Open Lease project
'''
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 Open Lease
#
#
#              open source rental property management software
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# (c) 2007 Jason McVetta
#
# This is Free Software.  It is realeased under the GNU Affero General Public
# License (version 3).  See http://www.fsf.org/licensing/licenses/agpl-3.0.html
# for more information.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# homepage:               http://openlease.org
# maintainer:             jason.mcvetta@gmail.com
# public git repository:  http://repo.or.cz/w/openlease.git
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~



#from django.conf.urls.defaults import *
from django.conf.urls.defaults import patterns
from django.conf.urls.defaults import include

urlpatterns = patterns('',
    # Example:
    # (r'^openlease/', include('openlease.foo.urls')),
     (r'^/', include('openlease.core.urls')),

    # Uncomment this for admin:
#     (r'^admin/', include('django.contrib.admin.urls')),
     (r'^admin/', include('django.contrib.admin.urls')),
)
