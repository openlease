'''
Core Data Models
'''
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#
#                                 Open Lease
#
#
#              open source rental property management software
#
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# (c) 2007 Jason McVetta
#
# This is Free Software.  It is realeased under the GNU Affero General Public
# License (version 3).  See http://www.fsf.org/licensing/licenses/agpl-3.0.html
# for more information.
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# homepage:               http://openlease.org
# maintainer:             jason.mcvetta@gmail.com
# public git repository:  http://repo.or.cz/w/openlease.git
#~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~


from django.db import models
from django.contrib.auth.models import User
from django.db import models
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes import generic


class Mgmt_Company(models.Model):
    '''
    A property management company.
    '''
    # We should have branding here, too
    pass


class Property_Manager(models.Model):
    '''
    A person manageing properties on behalf of a management company.  
    '''
    user = models.ForeignKey(User, unique=True, blank=False)
    company = models.ForeignKey(Mgmt_Company, blank=False)


class Building(models.Model):
    '''
    A residential structure containing one or more rental units.
    '''
    name = models.CharField(max_length=128)
    address = models.TextField()
    city = models.CharField(max_length=64)
    state_province = models.CharField(max_length=32)
    postal_code = models.CharField(max_length=32)
    num_units = models.IntegerField()


class Unit(models.Model):
    '''
    A single rental unit.
    '''
    building = models.ForeignKey(Building, blank=False)
    apartment = models.CharField(max_length=32, blank=False) # Unit number


class DeliverableItem(models.Model):
    '''
    A generic deliverable, necessary for a lease to take effect.
    '''
    short_name = models.SlugField(blank=False)
    full_name = models.CharField(max_length=32)
    description = models.TextField()
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')

    def __unicode__(self):
        return self.short_name


class Lease_Template(models.Model):
    '''
    A template specifying legal language of a lease contract, and the conditions
    & documentation which must be met for the lease to take effect.
    '''
    short_name = models.SlugField(blank=False)
    full_name = models.CharField(max_length=32)
    contract = models.TextField(blank=False)
    deliverables = models.ManyToManyField(DeliverableItem)

    def __unicode__(self):
        return self.short_name


class Lease(models.Model):
    '''
    A contract binding one or more tenants to a single rental unit.
    '''
    # The only difference between a lease and a sublease is the master tenant
    #master_tenant = models.ForeignKey(Tenant, blank=False)
    RENT_FREQUENCIES = [('m', 'monthly'), ('b', 'bimonthly'), ('w', 'weekly')]
    template = models.ForeignKey(Lease_Template, blank=False)
    unit = models.ForeignKey(Unit, blank=False)
    start_date = models.DateField()
    end_date = models.DateField()
    rent_amount = models.FloatField()
    rent_frequency = models.CharField(max_length=32,  choices=RENT_FREQUENCIES)
    # rent_due is the date the FIRST rental payment is due on this lease.  All subsequent 
    # due-dates will be calculated programmatically based on this value.
    rent_due = models.DateField()


class Tenant(models.Model):
    '''
    A person leasing a rental unit.
    '''
    user = models.ForeignKey(User, unique=True, blank=False)
    lease = models.ForeignKey(Lease, blank=False)
    percent_responsible = models.FloatField(blank=False, default=100)


class Sublease(models.Model):
    start_date = models.DateField()
    end_date = models.DateField()
    content_type = models.ForeignKey(ContentType)
    object_id = models.PositiveIntegerField()
    content_object = generic.GenericForeignKey('content_type', 'object_id')


class Subtenant(models.Model):
    '''
    A person subleasing a unit from a tenant.
    '''
    user = models.ForeignKey(User, unique=True, blank=False)

